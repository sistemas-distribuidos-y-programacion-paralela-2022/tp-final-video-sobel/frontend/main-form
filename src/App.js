import './App.css';
import React from 'react';
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import FormVideo from './components/FormVideo'
import ShowVideo from './components/ShowVideo'



function App() {
  return (
    <BrowserRouter>
        <Routes>
            <Route path="/form" element={<FormVideo/>}></Route>
            <Route path="/showVideo/:videoId/:mono" element={<ShowVideo/>}></Route>
            <Route path="/" element={<Navigate to="/form" />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;
