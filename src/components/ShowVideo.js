import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import axios from "axios";
import '../css/ShowVideo.css';

function ShowVideo(props) {
    const { videoId, mono } = useParams();
    const [videoUrl, setVideoUrl] = useState(null);
    const [videoReady, setVideoReady] = useState(false);

    async function getVideoRetriever() {
        const video_retriever_port = process.env.REACT_APP_PORT_VIDEO_RETRIEVER || 3000;
        let video_retriever_host = process.env.REACT_APP_HOST_VIDEO_RETRIEVER || '127.0.0.1';
        const response = await axios.get(`http://${video_retriever_host}:${video_retriever_port}/get-video-link?video-id=${videoId}&mono=${mono}`);
        if (response.data.url) {
            setVideoUrl(response.data.url);
            setVideoReady(true);
        }
    }

    useEffect(() => {
        let interval;

        if (!videoReady) {
            interval = setInterval(() => {
                getVideoRetriever();
            }, 5000);
        }

        return () => {
            clearInterval(interval);
        };
    }, [videoReady]);

    return (
        <div className="conteiner-video">
            <h1>Filtro Sobel</h1>
            <div className="cont-data">
                {videoUrl ? (
                    <>
                        <h2>Video Listo!!</h2>
                        <a class="link-video" href={videoUrl}>Haga click Aquí para ver su Video.</a>
                    </>
                ) : (
                    <div className="cont-load">
                        <img src="/load.gif" alt="Cargando..." className="load-gif"/>
                    </div>
                )}
                {!videoUrl && (
                    <>
                        <p className="label-loading">El video está siendo procesado. Por favor, espere...</p>
                    </>
                )}
            </div>
        </div>
    );
}

export default ShowVideo;
