import React, { useState } from "react";
import { Link } from "react-router-dom"; // Importa Link
import '../css/FormVideo.css';
import axios from "axios";

function FormVideo() {
    const [selectedFile, setSelectedFile] = useState(null);
    const [videoId, setVideoId] = useState(null);
    const [loading, setLoading] = useState(false);
    const [useCustomHost, setUseCustomHost] = useState(false);
    const [mono, setMono] = useState(false)

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        setSelectedFile(file);
    };

    const handleUpload = async (event) => {
        event.preventDefault();
        if (selectedFile) {
            setLoading(true); // Inicia la carga

            const video_reciver_port = process.env.REACT_APP_PORT_VIDEO_RECEIVER || 5555;
            let video_reciver_host = process.env.REACT_APP_HOST_VIDEO_RECEIVER || '127.0.0.1';
            let monolitic_host = process.env.REACT_APP_HOST_MONOLITHIC || '127.0.0.1';
            let path = 'videoreceiver';

            if (useCustomHost) {
                // Usa la URL monolitica cuando el checkbox está marcado
                video_reciver_host = monolitic_host; 
                path = 'sobel_mono';
            }

            const formData = new FormData();
            formData.append('video', selectedFile);

            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            };

            try {
                const url = `http://${video_reciver_host}:${video_reciver_port}/${path}`;
                const response = await axios.post(url, formData,config);

                const { videoId } = response.data;
                setVideoId(videoId);
                console.log('Video ID:', videoId);
                console.log('URL:', url);

            } catch (error) {
                console.error('Error:', error);
            } finally {
                setLoading(false); // Finaliza la carga
            }
        }
    };


    return (
        <div className="conteiner-form">
            <h1>Filtro Sobel</h1>
            <div className="form">
                <label>
                    <input
                        class="check"
                        type="checkbox"
                        checked={useCustomHost}
                        onChange={() =>{ 
                            setUseCustomHost(!useCustomHost);
                            // Asigna o remueve la clase al botón con ID 'btn' al cambiar el estado del checkbox
                            const btn = document.getElementById('btn');
                            btn && (useCustomHost ? btn.classList.remove('color-mon') : btn.classList.add('color-mon'));
                            setMono(!useCustomHost);
                        }}
                    />
                    Usar URL del Monolitico
                </label>
                <input type="file" onChange={handleFileChange} className="form-input" />
                <button id="btn" onClick={handleUpload} className="form-submit">Procesar Video</button>
                {loading && (
                // Mostrar el GIF de carga mientras se está procesando
                    <div class="cont-load">
                        <img src="/load.gif" alt="Cargando..." class="load-gif"/>
                    </div>
                )}
                {videoId && !loading && (
                    // Enlace a la página VideoReceiver con el ID del video en la URL
                    <Link to={`/showVideo/${videoId}/${mono}`} className="video-id-link">
                        Ver estado del video (ID: {videoId})
                    </Link>
                )}
            </div>
        </div>
    );
}

export default FormVideo;

