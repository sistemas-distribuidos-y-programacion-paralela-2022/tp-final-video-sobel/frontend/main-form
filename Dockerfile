# Usar una imagen base de Node.js
FROM node:18

# Se crea un nuevo grupo de sistema(flag -r) llamado "user" 
RUN groupadd -r user

# Se crea un nuevo usuario de sistema(flag -r) y se agrega al grupo creado previamente (flag -g)
RUN useradd -r -g user user

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Se cambian los npermisos del workdir al usuario no root "user"
RUN chown -R user:user /app

# Copiar los archivos de la aplicación al directorio de trabajo
COPY package*.json ./
RUN npm install

# Copiar el resto de los archivos de la aplicación
COPY . .

# Se cambia al usuario no root "user"
USER user 

# Exponer el puerto en el que se ejecutará la aplicación
EXPOSE 8080

# Comando para iniciar la aplicación cuando se inicie el contenedor
CMD ["npm", "start"]
